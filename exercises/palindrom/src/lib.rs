/// Compute the int palindrom
///
/// # Example:
/// ```
/// assert_eq!(palindrom::compute(0), 0);
/// assert_eq!(palindrom::compute(100), 1);
/// assert_eq!(palindrom::compute(1234), 4321);
/// assert_eq!(palindrom::compute(555555), 555555);
/// ```
pub fn compute(x: u64) -> u64 {
    let mut x: u64 = x;
    let mut y: u64 = 0;

    while x > 0 {
        y = y * 10 + x % 10;
        x = (x - x % 10) / 10;
    }
    y
}
