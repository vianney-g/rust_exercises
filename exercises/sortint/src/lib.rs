use std::cmp;

/// Sort int by cipher
///
/// # Examples
/// ```
/// assert_eq!(sortint::v1(58264), 86542);
/// assert_eq!(sortint::v1(0), 0);
/// assert_eq!(sortint::v1(1234567890), 9876543210);
/// assert_eq!(sortint::v1(9876543210), 9876543210);
/// ```
pub fn v1(x: u64) -> u64 {
    let mut ciphers: Vec<char> = x.to_string().chars().collect();
    ciphers.sort_by_key(|&num| cmp::Reverse(num));
    let _str: String = ciphers.into_iter().collect();
    _str.parse().unwrap()
}

/// Sort int by cipher. Another algorithm
///
/// # Examples
/// ```
/// assert_eq!(sortint::v2(58264), 86542);
/// assert_eq!(sortint::v2(0), 0);
/// assert_eq!(sortint::v2(1234567890), 9876543210);
/// assert_eq!(sortint::v2(9876543210), 9876543210);
/// ```
pub fn v2(x: u64) -> u64 {
    let mut ciphers: Vec<char> = x.to_string().chars().collect();
    ciphers.sort_by_key(|&num| cmp::Reverse(num));
    ciphers.into_iter().collect::<String>().parse().unwrap()
}
