/// A very secret language.
///
/// # Examples:
/// ```
/// assert_eq!(duplicate_encode::encode("din"), "(((");
/// assert_eq!(duplicate_encode::encode("recede"), "()()()");
/// assert_eq!(duplicate_encode::encode("Success"), ")())())", "should ignore case");
/// assert_eq!(duplicate_encode::encode("(( @"), "))((");
/// ```
pub fn encode(word: &str) -> String {
    let lower = word.to_lowercase();
    lower
        .chars()
        .map(|c| match lower.matches(c).count() {
            1 => '(',
            _ => ')',
        })
        .collect()
}
