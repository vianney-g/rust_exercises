/// Compute diff between arrays
///
/// # Examples
/// ```
///assert_eq!(array_diff::v1(vec![1, 2], vec![1]), vec![2]);
///assert_eq!(array_diff::v1(vec![1, 2, 2], vec![1]), vec![2, 2]);
///assert_eq!(array_diff::v1(vec![1, 2, 2], vec![2]), vec![1]);
///assert_eq!(array_diff::v1(vec![1, 2, 2], vec![]), vec![1, 2, 2]);
///assert_eq!(array_diff::v1(vec![], vec![1, 2]), vec![]);
/// ```
pub fn v1<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
    let mut res = a;
    for i in b {
        loop {
            match res.iter().position(|j| j == &i) {
                None => break,
                Some(j) => {
                    res.remove(j);
                    ()
                }
            }
        }
    }
    res
}

/// Compute diff between arrays
///
/// # Examples
/// ```
///assert_eq!(array_diff::v2(vec![1, 2], vec![1]), vec![2]);
///assert_eq!(array_diff::v2(vec![1, 2, 2], vec![1]), vec![2, 2]);
///assert_eq!(array_diff::v2(vec![1, 2, 2], vec![2]), vec![1]);
///assert_eq!(array_diff::v2(vec![1, 2, 2], vec![]), vec![1, 2, 2]);
///assert_eq!(array_diff::v2(vec![], vec![1, 2]), vec![]);
/// ```
pub fn v2<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
    let mut res = a;
    res.retain(|i| !b.contains(i));
    res
}

/// Compute diff between arrays
///
/// # Examples
/// ```
///assert_eq!(array_diff::v3(vec![1, 2], vec![1]), vec![2]);
///assert_eq!(array_diff::v3(vec![1, 2, 2], vec![1]), vec![2, 2]);
///assert_eq!(array_diff::v3(vec![1, 2, 2], vec![2]), vec![1]);
///assert_eq!(array_diff::v3(vec![1, 2, 2], vec![]), vec![1, 2, 2]);
///assert_eq!(array_diff::v3(vec![], vec![1, 2]), vec![]);
/// ```
pub fn v3<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
    a.into_iter().filter(|i| !b.contains(i)).collect()
}
