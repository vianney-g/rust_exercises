/// A strange sorter
///
/// # Example:
/// ```
/// assert_eq!(your_order_please::sort("is2 Thi1s T4est 3a"), "Thi1s is2 3a T4est");
/// assert_eq!(your_order_please::sort(""), "");
/// ```
pub fn sort(sentence: &str) -> String {
    let mut words: Vec<&str> = sentence.split_whitespace().collect();
    let the_digit = |s: &str| s.chars().filter(|c| c.is_ascii_digit()).next();
    words.sort_by_key(|w| the_digit(w));
    words.join(" ")
}
