/// Return a diamond of stars for the given size
///
/// # Examples:
/// ```
/// assert_eq!(diamonds::v1(3), Some(" *\n***\n *\n".to_string()));
/// assert_eq!(diamonds::v1(5), Some("  *\n ***\n*****\n ***\n  *\n".to_string()));
/// assert_eq!(diamonds::v1(-3), None);
/// assert_eq!(diamonds::v1(2), None);
/// assert_eq!(diamonds::v1(0), None);
/// assert_eq!(diamonds::v1(1), Some("*\n".to_string()));
/// ```
pub fn v1(n: i32) -> Option<String> {
    if n < 1 || n % 2 == 0 {
        return None;
    }
    let mut res = String::new();
    for i in (1..=n).step_by(2) {
        res += " ".repeat(((n - i) / 2) as usize).as_str();
        res += "*".repeat(i as usize).as_str();
        res.push_str("\n");
    }
    for i in (3..=n).step_by(2) {
        res += " ".repeat((i / 2) as usize).as_str();
        res += "*".repeat((n - i + 1) as usize).as_str();
        res.push_str("\n");
    }
    Some(res)
}

/// Return a diamond of stars for the given size
///
/// # Examples:
/// ```
/// assert_eq!(diamonds::v2(3), Some(" *\n***\n *\n".to_string()));
/// assert_eq!(diamonds::v2(5), Some("  *\n ***\n*****\n ***\n  *\n".to_string()));
/// assert_eq!(diamonds::v2(-3), None);
/// assert_eq!(diamonds::v2(2), None);
/// assert_eq!(diamonds::v2(0), None);
/// assert_eq!(diamonds::v2(1), Some("*\n".to_string()));
/// ```
pub fn v2(n: i32) -> Option<String> {
    if n < 1 || n % 2 == 0 {
        return None;
    }
    let n = n as usize;
    let mut res = "*".repeat(n) + "\n";
    for i in (2..=n - 1).step_by(2) {
        let _line = format!("{}{}\n", " ".repeat(i / 2), "*".repeat(n - i).as_str());
        res = format!("{}{}{}", _line, res, _line);
    }
    Some(res)
}

/// Return a diamond of stars for the given size
///
/// # Examples:
/// ```
/// assert_eq!(diamonds::v3(3), Some(" *\n***\n *\n".to_string()));
/// assert_eq!(diamonds::v3(5), Some("  *\n ***\n*****\n ***\n  *\n".to_string()));
/// assert_eq!(diamonds::v3(-3), None);
/// assert_eq!(diamonds::v3(2), None);
/// assert_eq!(diamonds::v3(0), None);
/// assert_eq!(diamonds::v3(1), Some("*\n".to_string()));
/// ```
pub fn v3(n: i32) -> Option<String> {
    if n < 1 || n % 2 == 0 {
        return None;
    }
    let n = n as usize;
    let diamond = (1..=n)
        .chain((1..n).rev())
        .step_by(2)
        .map(|i| format!("{}{}\n", " ".repeat((n - i) / 2), "*".repeat(i)))
        .collect();
    Some(diamond)
}
