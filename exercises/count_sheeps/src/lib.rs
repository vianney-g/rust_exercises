/// Count sheeps (true values) from an array of booleans.
///
/// # Examples
/// ```
/// assert_eq!(count_sheeps::count(&[false]), 0);
/// assert_eq!(count_sheeps::count(&[true]), 1);
/// assert_eq!(count_sheeps::count(&[true, false]), 1);
/// ```
pub fn count(sheep: &[bool]) -> u8 {
    sheep.into_iter().filter(|&k| *k).count() as u8
}
